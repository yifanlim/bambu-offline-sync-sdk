# Bambu Sync SDK
An API set to communicate with Bambu services that abstracts away the implementation of local storage for offline use.

## Overview
This SDK is a JS library for an RN app. Below is the big picture of how the SDK works:

![overview](https://i.imgur.com/SSgrlqr.png)

The SDK exposes Bambu endpoints as functions that upon called will check the device connectivity. If the device is connected to the internet the SDK will use server data, otherwise it will use data in Realm.

## Installation
Let's assume we are starting an RN project:
```
react-native init <project-name> 
```
To install the SDK, run this npm command:
```
npm install --save git+https://bitbucket.org/bambudeveloper/bambu-offline-sync-sdk.git
```
To integrate this SDK with your RN project, you need to install Realm JS version 2.2.15:
```
npm install --save realm@2.2.15
```
```
react-native link realm
```
This SDK accepts environment variable to set the endpoints, so if you want to set the endpoints at runtime we need to enable environment variable in the RN project. We can do it using this babel plugin:
```
npm install babel-plugin-transform-inline-environment-variables
```
After the plugin is installed, modify the projects' `.babelrc` to use the plugin:
```
{
  "presets": ["react-native"],
  "plugins": ["transform-inline-environment-variables"]
}

```
We can then set the API endpoints the SDK will use by setting environment variable on the start script like this:
```
"start": "BAMBU_BASE_URL=[YOUR_DESIRED_URL] node node_modules/react-native/local-cli/cli.js start",
```
This is the list of endpoints that can be set at compile time:

1. [TO BE DECIDED],
2. ...

## Features
At this early state, this SDK contains modules below:

1. Customer, contains methods for communicating with Bambu's customer microservice.
2. Sync, a module for sync-ing offline data with the server
3. Expense Calculator (online only, to be updated)
4. General Calculator (online only, to be updated)

## Usage
Let's take a look at how to use one of the module. We'll use `Customer` in this example.

This is one way to use the SDK in the RN code base:
```
import sdk from 'bambu-sync-sdk';

async getCustomers() {
  const { Customer } = sdk;
  const { getCustomers } = Customer;
  const { status, message, data, error } = await getCustomers();
  console.log(
    status, // 'OK'
    message, // 'Fresh server data is used. Stale local data is refreshed'
    data, // '[{ ...customerAttributes }]'
    error // null
  )
}
```

Module functions (except for Sync) like `Customer.getCustomers` return a promise which will always resolve into an object with attributes below:

1. `status`
2. `message`
3. `data`
4. `error`

### Important

Each element in the data (e.g customer data) will have either or both `id` and `localId` attributes. `id` attribute is given from the server, whereas `localId` is given by the SDK to enable the app developer to manipulate local data.

The functions exposed in the SDK will decide what to do based on which kind of id is given. For example if you call `deleteCustomer({ localId })` with local id as the parameter, the function will assume we're just deleting the local data without attempting to request delete to the server. This is useful to make a connection free transaction. If we call `createCustomer` without internet connection, the saved data will only have `localId` and that `localId` can be used to display, update, or delete the data without internet connection.

## Available API

### Customer

##### createCustomer(data)
```
const { Customer } = sdk;
const { createCustomer } = Customer;
const { status, message, data, error } = await createCustomer({
  firstName: 'Jane',
  lastName: 'Doe',
});
```
If the request is successful the data will be saved both locally and in the server, so the new customer data will have an `id` assigned from the server. But if the request failed the data will only be saved locally and the SDK will assigned a `localId` to it.

The `data` param is an object containing below customer data. Note that every attribute listed is optional.

1. `firstName`, accepts a `String`
2. `lastName`, accepts a `String`
3. `fullName`, accepts a `String`
4. `nationality`, accepts a `String`
5. `gender`, accepts `"male"` and `"female"`
6. `maritalStatus`, accepts `"single"` and `"married"`
7. `numberOfChildren`, accepts a `Number`
8. `birthDate`, accepts a date ISO string e.g `2018-07-02T05:33:41.948Z`
9. `age`, accepts a `Number`
10. `countryOfBirth`, accepts a `String`
11. `race`, accepts a `String`
12. `religion`, accepts a `String`
13. `ethnicOrigin`, accepts a `String`
14. `taxResidency`, accepts a `String`
15. `yearsOfResidency`, accepts a `String`
16. `height`, accepts a `Number`
17. `width`, accepts a `Number`
18. `interests`, accepts a `String`
19. `sourceoFFunds`, accepts a `String`
20. `purposeOfInvestment`, accepts a `String`
21. `householdIncome`, accepts a `Number`
22. `personalIncome`, accepts a `Number`
23. `additionalIncome`, accepts a `Number`
24. `riskAppetite`, accepts a `String`
25. `status`, accepts a `String`
26. `agentId`, accepts a `Number`

##### getCustomers()
```
const { Customer } = sdk;
const { getCustomers } = Customer;
const { status, message, data, error } = await getCustomers();
```

##### getCustomerById({ id, localId })
```
const { Customer } = sdk;
const { getCustomerById } = Customer;
const { status, message, data, error } = await getCustomerById({
  id: 1, // accepts either id or localId, can't accept both
});
```

##### updateCustomer(data)
```
const { Customer } = sdk;
const { updateCustomer } = Customer;
const { status, message, data, error } = await updateCustomer({
  id: 2, // accepts either id or localId, can't accept both
  firstName: 'Another Jane',
});
```

##### deleteCustomer({ id, localId })
```
const { Customer } = sdk;
const { deleteCustomer } = Customer;
const { status, message, data, error } = await deleteCustomer({
  id: 2, // accepts either id or localId, can't accept both
});
```

### Other models like Goal are under development

### sync
Use sync to try to push all locally modified data to the server
```
const { sync } = sdk;
const { status, message, customerSyncData } = await sync();
```
This will command the SDK to go through all the changes that are saved locally and send them all to the server.

### Expense (online only, to be updated)

##### calculateRetirementGoal(payload)

Calculates the retirement expenses required based on income bands and questions. The parameter `payload` is an object which contains attributes below:

1. `countryInput`, accepts a `String`, the list of accepted countries can be retrieved from `getRetirementCountries`
2. `monthlyIncome`, accepts a `Number`
3. `currentYear`, accepts a `Number`
4. `age`, accepts a `Number`
5. `retirementAge`, accepts a `Number`
6. `inflationRate`, accepts a `Number`
7. `lifestyleQuestionArray`, accepts an Array of Number which represents a retirement lifestyle question (the questions and numberings are still to be defined)
8. `lifestyleAnswerArray`, accepts an Array of Number which represents a retirement lifestyle answer (the questions and numberings are still to be defined)

##### getRetirementCountries()

##### getRetirementDataByBand({ country, bandInput })

The list of accepted countries can be retrieved from `getRetirementCountries`

##### getExpendituresByCountry(country)

The list of accepted countries can be retrieved from `getRetirementCountries`

##### getIncomeBandsByCountry(country)

The list of accepted countries can be retrieved from `getRetirementCountries`

### GeneralCalculator  (online only, to be updated)

##### calculateHouseCost(payload)

Calculates prices of houses based on location and house type after adjustment to inflation. The parameter `payload` is an object which contains attributes below:

1. `downPaymentYear`, accepts a `Number`
2. `country`, accepts `"Singapore"`, `"Thailand"`, `"Indonesia"`, and `"Malaysia"`
3. `regionInput` (optional), accepts country's region (if exists). Example: `"East Kalimantan"` and `"Serawak"`
4. `cityInput` (optional), accepts region's city (if exists). Example: `"Bangkok"` and `"Denpasar"`
5. `locationInput` (optional), accepts country's location (if exists). Example: `"Geylang"` and `"Jurong East"`
6. `houseTypeInput`, accepts a `String`, the list of accepted house type can be retrieved from `getHouseTypeByCity({ country, city })`
7. `downPaymentPct`, accepts a `Number`
8. `inflationRate`, accepts a `Number`
9. `currentYear`, accepts a `Number`

##### getCityByRegion({ country, region })

##### getCountryData(country)

##### getHousePrice({ country, region, city, location, district, houseType, roomType })

##### getHouseTypeByCity({ country, city })

##### calculateEducationCost(payload)

Calculates cost of university for kids after adjusting for inflation to the year of child's education. The parameter `payload` is an object which contains attributes below:

1. `age`, accepts a `Number`
2. `ageOfUni`, accepts a `Number`
3. `maxGoalYear`, accepts a `Number`
4. `universityType`, accepts `"Public"` and `"Private"`
5. `medicineOption`, accepts `"Yes"` and `"No"`
6. `country`, accepts `"USA"`, `"Canada"`, `"United Kingdom"`, `"Australia"`, `"New Zealand"`, `"Singapore"`, `"Singapore(International Student)"`, `"Indonesia"`, `"Philippines"`, `"Vietnam"`, and `"Thailand"`
7. `inflationRate`, accepts a `Number`
8. `currentYear`, accepts a `Number`

##### getUniversityByCountry(country)

##### getUniversityPrice({ country, medicineOption, typeOption })